package main

import (
	"fmt"
	"huffman/huffman"
)

func main() {
	freq_chart := huffman.GetFreqChart("gazal.txt")

	for k, v := range freq_chart.FreqChart {
		fmt.Printf("%q: %d\n", k, v)
	}

	fmt.Println("----------------")
	fmt.Println(freq_chart.Nodes)

	freq_chart.BuildTree()
}
