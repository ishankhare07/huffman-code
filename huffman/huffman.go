package huffman

import (
	"fmt"
	"io/ioutil"
	"sort"
)

type HuffmanNode struct {
	Key   string
	Freq  int
	Next0 *HuffmanNode
	Next1 *HuffmanNode
}

func (h HuffmanNode) String() string {
	return fmt.Sprintf("%q: %d\n", h.Key, h.Freq)
}

//----------------------implement sort.Interface------------------//
type ByFreq []*HuffmanNode

func (n ByFreq) Len() int {
	return len(n)
}

func (n ByFreq) Swap(i, j int) {
	n[i], n[j] = n[j], n[i]
}

func (n ByFreq) Less(i, j int) bool {
	return n[i].Freq < n[j].Freq
}

//----------------------------------------------------------------//

type Chart struct {
	FreqChart map[string]int
	Nodes     []*HuffmanNode
}

func (c *Chart) BuildTree() *HuffmanNode {
	var p, n *HuffmanNode

	nodes := make([]*HuffmanNode, len(c.Nodes))
	copy(nodes, c.Nodes)

	for len(nodes) > 0 {
		//assign to first node
		p = nodes[0]
		if n == nil {
			// first node in tree
			n = p
		} else {
			temp := &HuffmanNode{Freq: (n.Freq + p.Freq)}

			if n.Freq <= temp.Freq {
				temp.Next0 = n
				temp.Next1 = p
			} else {
				temp.Next0 = p
				temp.Next1 = n
			}

			n = temp
		}

		// pop first element
		nodes = append(nodes[:0], nodes[1:]...)

		sort.Sort(ByFreq(c.Nodes))
	}

	return n
}

func GetFreqChart(filename string) Chart {
	chart := Chart{FreqChart: make(map[string]int)}

	s, err := ioutil.ReadFile("gazal.txt")

	if err != nil {
		panic(err.Error())
	}

	// populate map
	for _, c := range s {
		_, ok := chart.FreqChart[string(c)]

		if !ok {
			// value does not exists
			chart.FreqChart[string(c)] = 1
		} else {
			// value already exists
			chart.FreqChart[string(c)] += 1
		}
	}

	// fillup list
	for k, v := range chart.FreqChart {
		chart.Nodes = append(chart.Nodes, &HuffmanNode{
			Key:  k,
			Freq: v,
		})
	}

	// sort list
	sort.Sort(ByFreq(chart.Nodes))

	return chart
}
